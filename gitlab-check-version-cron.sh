#!/bin/sh

set -e

DIR=$(dirname $(readlink -f "$0") )
. $DIR/config.sh
export GITLAB_VERSION=$( curl -sSfH "PRIVATE-TOKEN: $TOKEN" "$GITLAB_HOST/api/v4/version" | jq -r .version | cut -d. -f1-2 )

if ! expr "$GITLAB_VERSION" : '[0-9]\+\.[0-9]\+' >/dev/null; then
	echo "Returned Gitlab version '$GITLAB_VERSION' doesn't match version regex"
	exit 1
fi

envsubst '$GITLAB_VERSION' < $DIR/pin-gitlab-runner.pref.tpl > $DIR/pin-gitlab-runner.pref

