This is a set of scripts/config files to make sure the gitlab-runner is kept up to date with the Gitlab instance version.

* gitlab-check-version-cron.sh should be symlinked in /etc/cron.daily or otherwise run daily or at least weekly. It will check the gitlab version and update the apt pinning file appropriately, as to only allow package versions matching the instance to be installed.
* pin-gitlab-runner.pref is the file that is produced by the above script from its .tpl, and it should be symlinked to /etc/apt/preferences.d/. It assigns a pin-priority of 990 to packages of the appropriate version and 50 to the rest.
* apt-unattended-upgrades.conf should be symlinked to /etc/apt/apt.conf.d/55unattended-upgrades-gitlab-runner (or any name starting a number > 50). It includes the gitlab packages origin in the unattended-upgrades whitelist.
* config.sh.sample should be copied/renamed to config.sh and filled to contain the Gitlab access token and instance domain. The token should be a personal access token with the read_api permission.
